import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Ingredient} from '../../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list.service';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  @ViewChild('form') ngForm: NgForm;
  editMode = false;
  editedItem: Ingredient;
  editedItemIndex: number;
  subscription: Subscription;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.subscription = this.shoppingListService.startedEditing.subscribe((index: number) => {
      this.editMode = true;
      this.editedItemIndex = index;
      this.editedItem = this.shoppingListService.getOne(index);
      this.ngForm.setValue({
        name: this.editedItem.name,
        amount: this.editedItem.amount
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmitForm(form: NgForm) {
    const ingredient = new Ingredient(form.value.name, +form.value.amount);
    this.shoppingListService.save(ingredient, this.editedItemIndex);
    this.onResetForm();
  }

  onResetForm() {
    this.editMode = false;
    this.editedItemIndex = null;
    this.ngForm.reset();
  }

  onDeleteForm() {
    this.shoppingListService.delete(this.editedItemIndex);
    this.onResetForm();
  }
}
