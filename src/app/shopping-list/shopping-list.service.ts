import {Ingredient} from '../shared/ingredient.model';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class ShoppingListService {

  ingredientsChanged = new Subject<Ingredient[]>();
  startedEditing = new Subject<number>();

  private ingredients: Ingredient[] = [
    new Ingredient('Carrots', 2),
    new Ingredient('Tomatoes', 6)
  ];

  save(ingredient: Ingredient, index?: number) {
    if (index != null) {
      this.ingredients[index] = ingredient;
    } else {
      this.ingredients.push(ingredient);
    }
    this.ingredientsChanged.next(this.getAll());
  }

  getAll() {
    return this.ingredients.slice();
  }

  delete(index: number) {
      this.ingredients.splice(index, 1);
      this.ingredientsChanged.next(this.getAll());
  }

  exists(ingredient: Ingredient) {
    return this.findIndex(ingredient) >= 0;
  }

  findIndex(ingredient: Ingredient) {
    return this.ingredients.findIndex((item) => item.name === ingredient.name);
  }

  getOne(index: number) {
    return this.ingredients[index];
  }

}
