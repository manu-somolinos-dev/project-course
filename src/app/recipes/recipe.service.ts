import {Recipe} from './recipe.model';
import {Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';
import {Subject} from 'rxjs';
import {RemoteRecipesService} from '../remote-recipes.service';

@Injectable({providedIn: 'root'})
export class RecipeService {

  recipesChanged = new Subject<Recipe[]>();

  constructor(private shoppingListService: ShoppingListService, private remoteRecipesService: RemoteRecipesService) { }

  private recipes: Recipe[] = [
    new Recipe(
      'Spaghetti', 'Spaghetti Bolognese', 'https://c1.staticflickr.com/5/4152/4845012904_bdec726275_b.jpg',
      [
        new Ingredient('Pasta', 1),
        new Ingredient('Tomatoes', 5),
        new Ingredient('Beef', 2)
      ]
    ),
    new Recipe('Ribs', 'BBQ ribs', 'http://sphm-finder-site-production.s3-ap-southeast-1.amazonaws.com/2018/06/BBQribs-750x500.jpg',
      [
        new Ingredient('Ribs', 12),
        new Ingredient('BBQ Sauce', 2),
        new Ingredient('Fries', 8)
      ])
  ];

  getAll() {
    return this.recipes.slice();
  }

  getOne(index: number) {
    return this.getAll()[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    ingredients.forEach((ingredient) => this.shoppingListService.save(ingredient));
  }

  save(recipe: Recipe, index?: number) {
    if (index != null) {
      this.recipes[index] = recipe;
    } else {
      this.recipes.push(recipe);
    }
    this.recipesChanged.next(this.getAll());
  }

  delete(id: number) {
    this.recipes.splice(id, 1);
    this.recipesChanged.next(this.getAll());
  }

  persistData() {
    return this.remoteRecipesService.putAll(this.getAll());
  }

  fetchRemoteData() {
    this.remoteRecipesService.fetchAll().subscribe(
      (response) => {
        this.recipes = response;
        this.recipesChanged.next(this.getAll());
      },
      (error) => console.log('error', error)
    );
  }
}
