import {Component, OnDestroy, OnInit} from '@angular/core';
import {Recipe} from '../recipe.model';
import {RecipeService} from '../recipe.service';
import {Subscription} from 'rxjs';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {

  recipes: Recipe[];
  recipesSubscription: Subscription;

  constructor(private recipeService: RecipeService, private authService: AuthService) { }

  ngOnInit() {
    this.recipes = this.recipeService.getAll();
    this.recipesSubscription = this.recipeService.recipesChanged.subscribe(
      (list: Recipe[]) => this.recipes = list
    );
  }

  ngOnDestroy() {
    this.recipesSubscription.unsubscribe();
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}
