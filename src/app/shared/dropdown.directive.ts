import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appDrowdown]'
})
export class DropdownDirective {

  /* Alternative Mouseover/Mouseleave
  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.renderer.addClass(this.elementRef.nativeElement, 'open');
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.renderer.removeClass(this.elementRef.nativeElement, 'open');
  }
  */

  /** Alternative click (open) -> next click (close) */
  @HostBinding('class.open') isOpen = false;

  @HostListener('click') toggleOpen() {
    this.isOpen = !this.isOpen;
  }
}
