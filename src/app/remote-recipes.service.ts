import {Injectable} from '@angular/core';
import {Recipe} from './recipes/recipe.model';
import {map} from 'rxjs/operators';
import {AuthService} from './auth/auth.service';
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RemoteRecipesService {

  private url = 'https://ng-recipe-book-a17bd.firebaseio.com/recipes.json';

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  putAll(recipes: Recipe[]) {
    const headers = new HttpHeaders();
    /* headers usage sample
    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer some-token')
      .append('content-type', 'application/json')
      .delete('Authorization');
    */

    // it sends some events: (0. Sent -> 2. Response)
    return this.httpClient.put(this.url, recipes, {
      observe: 'events',
      headers: headers
    });

    // listen progress events --> Downloads and Uploads
    // you get three events --> 0: Sent --> 1: Upload Progress Event --> 3: Download Progress Event
    /*
    const httpRequest = new HttpRequest('PUT', 'https://recipes-storage-2367c.firebaseio.com/recipes.json', recipes, {
      reportProgress: true,
      params: params
    });
    return this.httpClient.request(httpRequest);
    */
  }

  fetchAll() {
    return this.httpClient.get<Recipe[]>(this.url)
      .pipe(
        map((recipes) => {
          for (let recipe of recipes) {
            if (!recipe['ingredients']) {
              recipes['ingredients'] = [];
            }
          }
          return recipes;
        })
      );
    /* consider next option just casting result
    return this.httpClient.get('https://recipes-storage-2367c.firebaseio.com/recipes.json?auth=' + token, {
        // body: {},
        headers: {},
        observe: 'response',
        responseType: 'text'    // options: json, blob (picture), arraybuffer, etc.
      })
      .pipe(
        map((recipes) => {
          console.log(recipes);
          return [];
        })
      );
      */
  }
}
