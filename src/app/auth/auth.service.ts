import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token: string;

  signupStatus = new Subject<{success: boolean, message: string}>();
  signinStatus = new Subject<{success: boolean, message: string}>();

  constructor(private router: Router) { }

  signup(email: string, password: string) {
    firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, password)
      .then( response => this.signupStatus.next({success: true, message: null}))
      .catch(error => this.signupStatus.error({success: false, message: error.message}));
  }

  signin(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(response => {
        this.router.navigate(['/']);
        firebase.auth().currentUser.getIdToken()
          .then((token: string) => {
            this.token = token;
            this.signinStatus.next({success: true, message: null});
          });
      })
      .catch(error => {
        console.log('error', error);
        this.signinStatus.error({success: false, message: error.message});
      });
  }

  signout() {
    firebase.auth().signOut();
    this.token = null;
  }

  getToken() {
    return this.token;
  }

  isAuthenticated() {
    return this.token != null;
  }
}
