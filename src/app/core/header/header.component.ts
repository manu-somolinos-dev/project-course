import {Component, OnInit} from '@angular/core';
import {RecipeService} from '../../recipes/recipe.service';
import {AuthService} from '../../auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpEvent, HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private recipesService: RecipeService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onSaveData() {
    this.recipesService.persistData()
      .subscribe(
        (response: HttpEvent<Object>) => {
            console.log('success', response, response.type);
            // pay attention multiple events sent
            // other Events are also importants --> check HttpEventType constants
          },
        (error) => console.log('error', error)
      );
  }

  onFetchRemoteData() {
    this.recipesService.fetchRemoteData();
  }

  onLogout() {
    this.authService.signout();
    this.router.navigate(['/'], {relativeTo: this.route});
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}
