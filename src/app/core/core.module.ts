import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {AppRoutingModule} from '../app-routing.module';
import {AuthInterceptor} from '../shared/auth.interceptor';
import {LoggingInterceptor} from '../shared/logging.interceptor';

@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    AppRoutingModule,
    HeaderComponent
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},   // register interceptors
    {provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true} // register interceptors
  ]
  // providers
  // we can define services here (instead of app.module.ts), but we define them at our services @Injectable decorator
  // using next config: '{providedIn: 'root'}'
})
export class CoreModule {}
